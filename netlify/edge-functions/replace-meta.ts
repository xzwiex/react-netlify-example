// eslint-disable-next-line import/no-unresolved
import { Context } from 'https://edge.netlify.com';

type Meta = {
  title: string;
  description: string;
  image: string;
};

// Facebok not working properly
// facebookexternalhit | Facebot
const BOT_PATTERN =
  // eslint-disable-next-line max-len
  /(googlebot\/|bot|Googlebot-Mobile|Googlebot-Image|Google favicon|vkShare|Mediapartners-Google|bingbot|slurp|java|wget|curl|Commons-HttpClient|Python-urllib|libwww|httpunit|nutch|phpcrawl|msnbot|jyxobot|FAST-WebCrawler|FAST Enterprise Crawler|biglotron|teoma|convera|seekbot|gigablast|exabot|ngbot|ia_archiver|GingerCrawler|webmon |httrack|webcrawler|grub.org|UsineNouvelleCrawler|antibot|netresearchserver|speedy|fluffy|bibnum.bnf|findlink|msrbot|panscient|yacybot|AISearchBot|IOI|ips-agent|tagoobot|MJ12bot|dotbot|woriobot|yanga|buzzbot|mlbot|yandexbot|purebot|Linguee Bot|Voyager|CyberPatrol|voilabot|baiduspider|citeseerxbot|spbot|twengabot|postrank|turnitinbot|scribdbot|page2rss|sitebot|linkdex|Adidxbot|blekkobot|ezooms|dotbot|Mail.RU_Bot|discobot|heritrix|findthatfile|europarchive.org|NerdByNature.Bot|sistrix crawler|ahrefsbot|Aboundex|domaincrawler|wbsearchbot|summify|ccbot|edisterbot|seznambot|ec2linkfinder|gslfbot|aihitbot|intelium_bot|yeti|RetrevoPageAnalyzer|lb-spider|sogou|lssbot|careerbot|wotbox|wocbot|ichiro|DuckDuckBot|lssrocketcrawler|drupact|webcompanycrawler|acoonbot|openindexspider|gnam gnam spider|web-archive-net.com.bot|backlinkcrawler|coccoc|integromedb|content crawler spider|toplistbot|seokicks-robot|it2media-domain-crawler|ip-web-crawler.com|siteexplorer.info|elisabot|proximic|changedetection|blexbot|arabot|WeSEE:Search|niki-bot|CrystalSemanticsBot|rogerbot|360Spider|psbot|InterfaxScanBot|Lipperhey SEO Service|CC Metadata Scaper|g00g1e.net|GrapeshotCrawler|urlappendbot|brainobot|fr-crawler|binlar|SimpleCrawler|Livelapbot|Twitterbot|cXensebot|smtbot|bnf.fr_bot|A6-Indexer|ADmantX|Twitterbot|OrangeBot|memorybot|AdvBot|MegaIndex|SemanticScholarBot|ltx71|nerdybot|xovibot|BUbiNG|Qwantify|archive.org_bot|Applebot|TweetmemeBot|crawler4j|findxbot|SemrushBot|yoozBot|lipperhey|y!j-asr|Domain Re-Animator Bot|AddThis)/i;
type PathHandler = (pathSegments: string[], domain: string) => Promise<Partial<Meta>>;

const PRODUCTION_DOMAINS = ['clutchy.io'];

const API_URL = {
  production: 'https://cloud.api.clutchy.io',
  develop: 'https://cloud.api.clutchy.io',
  // develop: 'https://cloud.dev.api.clutchy.io',
};

const getApiUrl = (domain: string, url: string) => {
  const isProduction = PRODUCTION_DOMAINS.includes(domain);
  const apiUrl = isProduction ? API_URL.production : API_URL.develop;
  return `${apiUrl}/${url}`;
};

const getData = async (domain: string, url: string) => {
  const fullUrl = getApiUrl(domain, url);
  const response = await fetch(fullUrl);
  return response.json();
};

const getGame = async (domain: string, gameSlug: string) => {
  return getData(domain, `games/get/${gameSlug}`);
};

const getCollection = async (domain: string, collecitonSlug: string) => {
  return getData(domain, `collections/get/${collecitonSlug}`);
};

const getNft = async (domain: string, nftId: string) => {
  const fullUrl = getApiUrl(domain, 'nft/list');
  
  const params = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ nftIds: [nftId] }),
  }
  console.log('params:', JSON.stringify(params));
  const response = await fetch(fullUrl, params);
  return response.json();
}

const DEFAULT_META: Meta = {
  title: 'Clutchy | Sui’s Most Anticipated Marketplace for NFTs & Gaming',
  description: 'Sui NFT Marketplace for Art & Gaming',
  image: 'https://clutchy.io/og.png',
};

const DEFAULT_HANDLER: PathHandler = async () => DEFAULT_META;

type WithWidth = {
  width: number;
};

const collectionResolver =
  (emptyText: string, socialImageKey: string) => async (pathSegments: string[], domain: string) => {
    if (pathSegments[1]) {
      const slug = pathSegments[1];
      const collection = await getCollection(domain, slug);

      const collectionImg: string =
        collection?.imgSet?.sort((a: WithWidth, b: WithWidth) => b.width - a.width)[0]?.url || collection.img;

      const image = collection?.socialImages?.[socialImageKey] ?? collectionImg;
      return {
        ...(collection?.title && { title: `${collection.title} | ${emptyText}` }),
        ...(image && { image }),
        description: collection?.description,
      };
    }
    return {
      title: emptyText,
      image: `https://${domain}/${socialImageKey}-og.png`,
    };
  };

const marketplaceHandler = collectionResolver('Clutchy | Marketplace', 'marketplace')

const CDN_URL = 'https://cdn.clutchy.io/ipfs/{{IPFS_URL}}?img-quality=60&img-width=400&img-height=400';
const URL_HANDLERS: { [c: string]: PathHandler } = {
  launchpad: collectionResolver('Clutchy | Launchpad', 'launchpad'),
  marketplace: async (pathSegments: string[], domain: string) => {
    console.log('Handle path:', pathSegments);
    if (pathSegments[1] !== 'item') {
      return marketplaceHandler(pathSegments, domain);
    }
    const nftId = pathSegments[2];
    const nft = await getNft(domain, nftId);
    console.log('nft', JSON.stringify(nft));
    const name = nft?.[0].name;
    const img = nft?.[0].url;
    console.log('Found image for nft:', nftId, img);
    if (img) {
      const isIpfs = img.startsWith('ipfs://');
      if (isIpfs) {
        const ipfsUrl = img.replace('ipfs://', '');
        return {
          image: CDN_URL.replace('{{IPFS_URL}}', ipfsUrl),
          title: name ? `${name} | Clutchy | Marketplace` : 'Clutchy | Marketplace',
        };
      }
      return {
        image: img,
        title: name ? `${name} | Clutchy | Marketplace` : 'Clutchy | Marketplace',
      };
    }
    return {
      title: 'Clutchy | Marketplace',
      image: `https://${domain}/marketplace-og.png`,
    };
  },
  games: async (pathSegments: string[], domain: string) => {
    if (pathSegments[1]) {
      const slug = pathSegments[1];
      const game = await getGame(domain, slug);

      return {
        ...(game?.game?.name && { title: `${game.game.name} Clutchy | Sui Games` }),
        ...(game?.game?.imgUrl && { image: game?.game?.imgUrl }),
      };
    }
    return {
      title: 'Clutchy | Sui Games',
      image: `https://${domain}/games-og.png`,
    };
  },
};

const replaceMeta = (content: string, meta: Meta) => {
  const { title, description, image } = meta;
  return content
    .replaceAll('Clutchy | Sui NFT Marketplace', title)
    .replaceAll('https://clutchy.io/og.png', image)
    .replaceAll('Sui NFT Marketplace for Art & Gaming', description);
};

const STATIC_FILE_REGEX = /\.(png|jpg|webp|json|css|ico)$/;

const handler = async (request: Request, context: Context) => {
  if (!BOT_PATTERN.test(request.headers.get('user-agent') ?? '')) {
    return;
  }
  // Just return what was requested without transforming it,
  // unless we fnd the query parameter for this demo
  const url = new URL(request.url);

  const domain = url.hostname;

  if (!url.pathname || STATIC_FILE_REGEX.test(url.pathname)) {
    return;
  }

  const pathSegments = url.pathname.split('/').filter(Boolean);

  const pathStart = pathSegments[0];

  const urlHandler = URL_HANDLERS[pathStart] || DEFAULT_HANDLER;

  const [meta, response] = await Promise.all([urlHandler(pathSegments, domain), context.next()]);

  const page = await response.text();

  const newContent = replaceMeta(page, { ...DEFAULT_META, ...meta });

  return new Response(newContent, response);
};

export default handler;
